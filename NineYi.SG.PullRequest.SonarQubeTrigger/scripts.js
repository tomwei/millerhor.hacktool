// ==UserScript==
// @name         NineYi.SG.PullRequest.SonarQubeTrigger
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       MillerHor
// @include      /^https://bitbucket\.org/nineyi/.*/pull-requests/\d+/*/
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';
// Your code here...
function init(){
    var jenkinsHost = 'http://ci3.91dev.tw:8080';
    //// Don't modify token value
    var token = '6CE7CE9FB713D88B4CEF86CBC2B98';
    var locationUrlPath = location.href.replace('https://').split('/');
    var repoName = locationUrlPath[2];
    var jenkinsJob = '/job/' + repoName; 

    var triggerBuildButton = $('<button class="aui-button aui-button-primary"><span>Trigger CI SonarQube Check</span></button>');
    triggerBuildButton.css('float', 'right');
    triggerBuildButton.click(function(){
        var branch = $('#id_source_group .branch.unabridged a').text();
        if(branch == null || branch.length == 0){
            branch = $('#id_source_group .branch.unabridged .branch-name').text()
        }
        
        if(branch == null || branch.length == 0 || branch == 'branch'){
            alert('can\'t get branch name');
            return;
        }
        
        var childWindow;
        //// auto login
        var buildPath = jenkinsJob + '/buildWithParameters?token=' + token + '&delay=0sec&BranchName=' + encodeURIComponent(branch);
        //alert('http://ci3.91dev.tw:8080/securityRealm/commenceLogin?from=' + encodeURIComponent(buildPath));
        childWindow = window.open('http://ci3.91dev.tw:8080/securityRealm/commenceLogin?from=' + encodeURIComponent(buildPath));
        childWindow.addEventListener('load', function(){
            alert('loaded');
            location.href = jenkinsHost + jenkinsJob
        }, true);
        
        setTimeout(function(){
            //// back to index
            childWindow.location.href = jenkinsHost + jenkinsJob;
        }, 3000);
    });

    $('#pull-request-header').append(triggerBuildButton);
}

(function(d){
    if(typeof($) === 'undefined'){
        var jq = document.createElement('script');
        jq.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
        head = document.getElementByTagName('head')[0];
        head.appendChild(jq);
    }
    var tmr = null;
    tmr = setInterval(function(){
        if(typeof($) != 'undefined'){
            init();
            clearInterval(tmr);
        }
    }, 1000);
})(document);