var nineYi = {
  sg: {
    pr: {
      selfCheckList: {
        version: '1.2',
        codeSmellList: [
          { rule: '代碼重複', info: '有一個以上的Method，程式碼超過50%相同' },
          { rule: '長方法', info: '一個Method超過50行' },
          { rule: '超大型物件', info: '一個Class超過500行' },
          { rule: '過多參數', info: '超過5個參數' },
          { rule: '拒絕繼承', info: '使用new關鍵字複寫父類別的屬性' },
          { rule: '人為的複雜', info: '沒有共用性、三行以內的程式碼，不需要抽method處理' },
          { rule: '命名過長', info: '超過五個單字' },
          { rule: '命名過短', info: '除三行以內的lambda以外，不要使用x, y...等' },
          { rule: 'God Line', info: '一行程式碼斷行超過三次' },
          { rule: '功能檢查', info: '是否完成預期功能，並針對案例進行驗證過' },
          { rule: 'Null物件檢查', info: '是否針對空值有進行null判斷及處理(預設值)' },
          { rule: 'RunStyleCop', info: 'StyleCop是否跑過，並且清空提示錯誤，檢查有無多餘的注釋、空格' },
          { rule: '估計異動資料數量', info: '是否有估計本次異動數量' }
        ],
        basicList: [
          { rule: 'SonarCube', info: '已執行掃描、並且已確認完所有建議修改項目' },
          { rule: 'PR目標正確', info: '目標branch正確，QA要進develop，PP要進release，專案要進Project' },
          { rule: '上線時間正確', info: '確認是這次Release要上線的項目，沒有偷跑' },
          { rule: 'Config檢查', info: 'QA、QA1...PP、Prod各環境該都要加上/修改。並且沒有機敏資料(APIKey、EncryptKey...等)' },
          { rule: 'EDMX檢查', info: '只有拉到這次修改範圍的欄位或只有改這次需求的範圍' },
          { rule: 'Cache檢查', info: '有調整到Cache內容(Redis、Memory、File)，並有更新Cache Key' }
        ],

        list: [
          'SonarCube掃描已執行',
          'SonarCube建議修改項目已調整完畢',
          '沒有衝突',
          '目標Branch正確',
          '本次修改需求的Release時間正確',
          'AppSettings.Config各環境都有新增/修改',
          'AppSettings.Config無機敏性資料(ApiKey、EncryptKey...等)',
          'EDMX只修改本次需求範圍內的欄位',
          '有調整Cache(Redis、Memory、File)內容，應更新Cache key',
          '程式碼只修改本次需求範圍內容'
        ]
      }
    }
  }
};