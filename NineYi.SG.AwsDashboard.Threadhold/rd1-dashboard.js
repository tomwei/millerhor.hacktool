// ==UserScript==
// @name         NineYi.SG.MWeb.AwsDashboard.Threadhold
// @namespace    https://bitbucket.org/MillerHor/millerhor.hacktool/
// @version      0.2
// @description  AWS Cloudwatch RD1 dashboard plugin, for threadhold alerm
// @author       MillerHor
// @match        https://ap-northeast-1.console.aws.amazon.com/cloudwatch/home?region=ap-northeast-1
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require      https://bitbucket.org/MillerHor/millerhor.hacktool/raw/master/NineYi.SG.AwsDashboard.Threadhold/script.js
// @require      https://bitbucket.org/MillerHor/millerhor.hacktool/raw/master/NineYi.SG.AwsDashboard.Threadhold/resource.mweb.js
// ==/UserScript==

(function() {
    'use strict';
    if(location.hash.startsWith("#dashboards:name=20181111-RD1") === false){return;}
    // Your code here...
    $(function(){
        beginAlerm(nineYi.sg.cloudwatch.mweb);
    });
})();